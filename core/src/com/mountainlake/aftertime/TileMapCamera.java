package com.mountainlake.aftertime;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Created by Lake on 20/12/2016.
 */
public class TileMapCamera extends OrthographicCamera {

    // Map tile size to round to
    private int tileSize;

    /**
     * Create a pixel-perfect camera for a map with the specified tile size
     * @param tileSize
     */
    public TileMapCamera(int tileSize){
        this.tileSize = tileSize;
    }

    /**
     * Sets camera position and updates camera
     * @param x X in tiles
     * @param y Y in tiles
     */
    public void setPosition(float x, float y){
        position.set(x, y, 0);
        update();
    }

    @Override
    public void update(){
        // Round position to avoid glitches
        float prevx = position.x;
        float prevy = position.y;
        position.x = (int)(position.x * tileSize) / (float)tileSize;
        position.y = (int)(position.y * tileSize) / (float)tileSize;
        super.update();
        position.set(prevx, prevy, 0);
    }
}
