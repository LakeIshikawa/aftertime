package com.mountainlake.aftertime;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;

/**
 * Created by Lake on 20/12/2016.
 */
public class Fader {

    // Fade color
    public Color fadeColor = new Color(0, 0, 0, 1);

    // Fade
    private float target;
    private float fadeTime = 0;
    private float time = 0;

    /**
     * Frame update
     * @param delta Delta time
     */
    public void update(float delta){
        if( fadeColor.r != target ) {
            float start = target == 1 ? 0 : 1;
            float value = Interpolation.fade.apply(start, target, time/fadeTime);
            fadeColor.set(value, value, value, 1);
            time += delta;

            if( time >= fadeTime ){
                fadeColor.a = target;
            }
        }
    }

    /**
     * Do a fade out
     */
    public void fadeOut(float fadeTime){
        this.fadeTime = fadeTime;
        target = 0;
        time = 0;
    }

    /**
     * Do a fade in
     */
    public void fadeIn(float fadeTime){
        this.fadeTime = fadeTime;
        target = 1;
        time = 0;
    }

    /**
     * @return Whether fading has finished
     */
    public boolean isFinished() {
        return fadeColor.r == target;
    }
}
