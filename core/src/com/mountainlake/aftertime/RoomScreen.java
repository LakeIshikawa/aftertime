package com.mountainlake.aftertime;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mountainlake.aftertime.actions.DynamicWindowAction;
import com.mountainlake.aftertime.actions.ScriptAction;
import com.mountainlake.aftertime.gameobject.Character;
import com.mountainlake.aftertime.gameobject.Enemy;
import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.gameobject.TileObject;
import com.mountainlake.aftertime.layers.GameMapLayer;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

/**
 * Created by Lake on 13/12/2016.
 */
public class RoomScreen implements Screen {
    // The viewports
    public Viewport mapViewport;
    public Viewport guiViewport;

    // Tiled map
    public TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private Color bgColor = Color.BLACK;

    // Graph
    public MapCollision mapCollision;

    // Text window
    public TextWindow textWindow;

    // Game objects in the room
    private SnapshotArray<GameObject> gameObjects = new SnapshotArray<>();

    // Layers
    private Array<GameMapLayer> layers = new Array<>();

    // Script actions
    public SnapshotArray<ScriptAction> actions = new SnapshotArray<>();

    // Character that has control
    public Character controlling;

    // Action zones
    public Array<ActionZone> actionZones = new Array<>();

    // Ongoing battle
    public Battle battle;

    // Debug
    private boolean debugMode = false;
    private FPSLogger fpsLogger = new FPSLogger();


    /**
     * Create a screen for playing the specified room
     * @param room A room to play. Must correspond to a .tmx asset
     */
    public RoomScreen(String room) {
        AtlasTmxMapLoader.AtlasTiledMapLoaderParameters parameters = new AtlasTmxMapLoader.AtlasTiledMapLoaderParameters();
        parameters.textureMinFilter = Texture.TextureFilter.Nearest;
        parameters.textureMinFilter = Texture.TextureFilter.Nearest;
        parameters.forceTextureFilters = true;
        map = new AtlasTmxMapLoader().load(room, parameters);
    }

    /**
     * @return Current map's tile width
     */
    public int getTileSize(){
        return map.getProperties().get("tilewidth", Integer.class);
    }

    // Map width and height
    public int getWidth(){
        return map.getProperties().get("width", Integer.class);
    }
    public int getHeight(){
        return map.getProperties().get("height", Integer.class);
    }

    /**
     * Add game object to the room
     * @param instanceName Instance name for script reference
     * @param go The game object
     */
    public void addObject(String instanceName, GameObject go) {
        gameObjects.add(go);
        AfterTime.i.globals.set(instanceName, CoerceJavaToLua.coerce(go));
    }

    /**
     * Remove game object from the room
     * @param object The object to remove
     */
    public void removeObject(GameObject object) {
        gameObjects.removeValue(object, true);
        AfterTime.i.globals.set(object.instanceName, LuaValue.NIL);
    }


    /**
     * Start an action
     * @param action The action to start
     */
    public void startAction(ScriptAction action) {
        actions.add(action);
        action.onStart();
    }

    /**
     * Show a dynamic window
     * @param x X position
     * @param y Y position
     * @param h Window's width
     * @param w Window's height
     */
    public void showDynamicWindow(String patch, float x, float y, float w, float h){
        startAction(new DynamicWindowAction(false, AfterTime.i.sprites.createPatch(patch), x, y, w, h));
    }

    /**
     * @return The scale ratio with which the map is being displayed
     */
    public float getMapPixelScale() {
        return guiViewport.getScreenHeight() / (mapViewport.getWorldHeight() * getTileSize());
    }

    @Override
    public void show() {
        Array<Polygon> walkableAreas = new Array<>();
        Array<Polygon> unwalkableAreas = new Array<>();

        int tileWidth = getTileSize();
        String hex = map.getProperties().get("backgroundcolor", String.class);
        if( hex != null ) {
            bgColor = Color.valueOf(hex);
        }

        // Create viewports
        mapViewport = new FitViewport(24, 18, new TileMapCamera(tileWidth));
        guiViewport = new ScreenViewport();
        // Bind system variables
        AfterTime.i.globals.set("camera", CoerceJavaToLua.coerce(mapViewport.getCamera()));
        AfterTime.i.globals.set("viewport", CoerceJavaToLua.coerce(mapViewport));

        // Load text window
        textWindow = new TextWindow(AfterTime.i.sprites.findRegion("system/window"), AfterTime.i.getFont("fnt_screen"), guiViewport);

        // Create renderer
        renderer = new OrthogonalTiledMapRenderer(map, 1.0f/tileWidth, AfterTime.i.batch);

        // Process events
        String startScript = null;

        // Process layers
        for(MapLayer layer : map.getLayers()){
            MapProperties pr = layer.getProperties();

            // Check for condition
            if( layer.getProperties().containsKey("condition") ){
                boolean pass = AfterTime.i.evalCondition((String) layer.getProperties().get("condition"));
                if( !pass ) {
                    layer.setVisible(false);
                    continue;
                }
            }

            // Set script object
            AfterTime.i.globals.set(layer.getName(), CoerceJavaToLua.coerce(layer));

            // Check layer type
            String type = layer.getProperties().get("type", "none", String.class);
            switch (type){
                case "events":
                    for( MapObject o : layer.getObjects() ){
                        float x = 0, y = 0;
                        RectangleMapObject rect = null;
                        if( o instanceof RectangleMapObject ){
                            rect = (RectangleMapObject) o;
                            x = rect.getRectangle().x/tileWidth + rect.getRectangle().width/(2*tileWidth);
                            y = rect.getRectangle().y/tileWidth + rect.getRectangle().height/(2*tileWidth);
                        }

                        // Parse object types and act accordingly
                        switch ( (String)o.getProperties().get("type")) {
                            case "start":
                                mapViewport.getCamera().position.set(x, y, 0);
                                mapViewport.getCamera().update();
                                startScript = (String) o.getProperties().get("script");
                                break;

                            case "setchar":
                                String instanceName = o.getProperties().get("char", String.class);
                                int facing = o.getProperties().get("facing", 0, Integer.class);
                                Character c = AfterTime.i.getCharacter(instanceName);
                                c.position.set(x, y);
                                c.face(facing);
                                gameObjects.add(c);
                                break;

                            case "spawnEnemy":
                                instanceName = (String) o.getProperties().get("instanceName");
                                String classpath = o.getProperties().get("enemy", String.class);
                                facing = o.getProperties().get("facing", 0, Integer.class);
                                Enemy enemy = AfterTime.i.loadEnemy(instanceName, classpath);
                                enemy.position.set(x, y);
                                enemy.face(facing);

                                gameObjects.add(enemy);
                                break;

                            case "spawnObject":
                                instanceName = (String) o.getProperties().get("instanceName");
                                String entity = o.getProperties().get("entity", String.class);
                                String objScript = o.getProperties().get("script", "", String.class);
                                float z = o.getProperties().get("z", 0f, Float.class);
                                GameObject go = new GameObject(instanceName, entity);
                                go.position.set(x, y);
                                go.z = z;

                                addObject(instanceName, go);

                                // Behavior
                                if( !objScript.equals("") ){
                                    AfterTime.i.loadObjectScript(go, Gdx.files.internal(objScript).readString());
                                }
                                break;

                            case "spawnScript":
                                String script = Gdx.files.internal((String) o.getProperties().get("script")).readString();
                                script = "local x="+x+"\nlocal y ="+y+"\n"+script;
                                AfterTime.i.evalScript(script);
                                break;

                            case "action":
                                script = o.getProperties().get("script", String.class);
                                Rectangle tileRect = new Rectangle(rect.getRectangle());
                                tileRect.x /= tileWidth;
                                tileRect.y /= tileWidth;
                                tileRect.width /= tileWidth;
                                tileRect.height /= tileWidth;
                                actionZones.add(new ActionZone(tileRect, script));
                                break;
                        }
                    }
                    break;

                case "collision":
                    for (MapObject o : layer.getObjects()) {
                        PolygonMapObject pl = (PolygonMapObject) o;
                        Polygon p = pl.getPolygon();
                        p.setPosition(pl.getPolygon().getX() / tileWidth, pl.getPolygon().getY() / tileWidth);
                        p.setScale(1.0f / tileWidth, 1.0f / tileWidth);
                        boolean walkable = pl.getProperties().get("walkable", true, Boolean.class);
                        if( walkable ) walkableAreas.add(p);
                        else unwalkableAreas.add(p);
                    }
                    break;

                case "object":
                    TiledMapTileLayer l = (TiledMapTileLayer) layer;
                    l.setVisible(false);
                    TileObject o = new TileObject(l, renderer);

                    if( layer.getProperties().containsKey("y") ) {
                        float y = layer.getProperties().get("y", Float.class);
                        o.position.set(0, l.getHeight() - y);
                    }

                    gameObjects.add(o);
                    break;

                case "none":
                    if( layer instanceof  TiledMapTileLayer ) {
                        l = (TiledMapTileLayer) layer;
                        GameMapLayer gameLayer = new GameMapLayer(l);

                        gameLayer.scrollSpeed.set(
                                pr.get("scroll_x", 0f, Float.class),
                                pr.get("scroll_y", 0f, Float.class));
                        gameLayer.wrapPoint.set(
                                pr.get("wrap_x", 0f, Float.class),
                                pr.get("wrap_y", 0f, Float.class));

                        layers.add(gameLayer);
                    }
                    break;
            }
        }

        // Create map graph
        mapCollision = new MapCollision(getWidth(), getHeight(), walkableAreas, unwalkableAreas);

        // Execute start script
        if (startScript != null) {
            AfterTime.i.evalScript(startScript);
        }

        // Start all game object scripts
        for(GameObject go : gameObjects) {
            go.triggerEvent("start");
            go.triggerEvent("update");
        }
    }

    @Override
    public void render(float delta) {
        // Debug
        if( Gdx.input.isKeyJustPressed(Input.Keys.NUM_1) ) {
            debugMode = !debugMode;
            Gdx.app.setLogLevel(debugMode ? Application.LOG_DEBUG : Application.LOG_INFO);
        }

        // Control the character
        if( controlling != null ){
            updateControls(delta);
        }

        // Update the script actions
        Object[] acs = actions.begin();
        for (int i = 0, n = actions.size; i < n; i++) {
            ScriptAction action = (ScriptAction) acs[i];
            if( action.update(delta) ) {
                actions.removeValue(action, true);
            }
        }
        actions.end();

        // Render the scene
        Gdx.gl.glClearColor(bgColor.r, bgColor.g, bgColor.b, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Fader
        AfterTime.i.fader.update(delta);
        AfterTime.i.batch.setColor(AfterTime.i.fader.fadeColor);

        // -- RENDER WITH MAP VIEWPORT
        mapViewport.apply();

        // Render layers
        for( int i=0; i<layers.size; i++ ) {
            GameMapLayer layer = layers.get(i);
            layer.update(delta);
        }
        renderer.setView((OrthographicCamera) mapViewport.getCamera());
        renderer.render();

        AfterTime.i.batch.setProjectionMatrix(mapViewport.getCamera().combined);
        AfterTime.i.batch.begin();

        // Sort by y!
        gameObjects.sort();
        // Render game objects
        Object[] gos = gameObjects.begin();
        for (int i = 0, n = gameObjects.size; i < n; i++) {
            GameObject object = (GameObject) gos[i];
            if( !object.update(delta) ) {
                object.render(AfterTime.i.batch, getTileSize());
            } else {
                // Destroy
                gameObjects.removeValue(object, true);
            }
        }
        gameObjects.end();
        AfterTime.i.batch.end();

        // Shapes
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        AfterTime.i.shapeRenderer.setProjectionMatrix(mapViewport.getCamera().combined);
        AfterTime.i.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        if( debugMode ) {
            mapCollision.renderCollision(AfterTime.i.shapeRenderer, controlling);

            // Action shapes
            for( int i=0; i<actions.size; i++ ){
                ScriptAction action = actions.get(i);
                action.onRenderDebug(AfterTime.i.shapeRenderer);
            }
        }
        AfterTime.i.shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);



        // -- RENDER WITH GUI VIEWPORT
        guiViewport.apply(true);
        AfterTime.i.batch.setProjectionMatrix(guiViewport.getCamera().combined);
        AfterTime.i.batch.begin();

        // Actions
        for( int i=0; i<actions.size; i++ ){
            ScriptAction action = actions.get(i);
            action.onRenderGUI(AfterTime.i.batch);
        }

        // Window
        textWindow.update(delta);
        textWindow.renderGUI(AfterTime.i.batch);

        // Battle gui
        if( battle != null ) battle.renderGUI(AfterTime.i.batch);

        AfterTime.i.batch.end();

        // Log fps
        if(debugMode){
            fpsLogger.log();
        }
    }

    /**
     * Control character
     */
    private void updateControls(float delta) {
        boolean l = Gdx.input.isKeyPressed(Input.Keys.LEFT);
        boolean r = Gdx.input.isKeyPressed(Input.Keys.RIGHT);
        boolean u = Gdx.input.isKeyPressed(Input.Keys.UP);
        boolean d = Gdx.input.isKeyPressed(Input.Keys.DOWN);

        // Speed
        controlling.speed.setZero();
        if( l ) controlling.speed.x = -controlling.characterClass.walkSpeed;
        else if( r ) controlling.speed.x = controlling.characterClass.walkSpeed;
        if( u ) controlling.speed.y = controlling.characterClass.walkSpeed;
        else if( d ) controlling.speed.y = -controlling.characterClass.walkSpeed;

        // Animation
        if( l ) controlling.facing = 2;
        else if( r ) controlling.facing = 0;
        else if( u ) controlling.facing = 1;
        else if( d ) controlling.facing = 3;
        if( l || r || u || d ) {
            controlling.setDirectionalAnimation("walk");
        }

        // Stop walking
        if( !l && !r && !d && !u ){
            controlling.setDirectionalAnimation("idle");
        }

        // Check for collision
        if( !controlling.speed.isZero() ) {
            mapCollision.collideChar(controlling, delta);
        }

        // Check for event triggers
        triggerEvents();

        // Camera follow
        cameraFollow();

        // Check for monster collision
        for( int i=0; i<gameObjects.size; i++ ){
            GameObject go = gameObjects.get(i);
            if( go instanceof Enemy ){
                Enemy enemy = (Enemy) go;
                if( Intersector.overlaps(controlling.worldCollision, go.worldCollision) ){
                    Array<Character> party = new Array<>(new Character[]{controlling});
                    Array<Enemy> enemies = new Array<>(new Enemy[]{enemy});
                    battle = new Battle(this, party, enemies);
                    battle.onStart();
                }
            }
        }
    }

    // Triggers any event to trigger
    private void triggerEvents() {
        boolean action = Gdx.input.isKeyJustPressed(Input.Keys.X);
        // Action zones
        for (int i = 0; i < actionZones.size; i++) {
            ActionZone zone = actionZones.get(i);
            if( action && zone.rect.contains(controlling.position) ){
                AfterTime.i.evalScript(zone.script);
            }
        }
    }

    // Follow character with camera
    private void cameraFollow() {
        Camera c = mapViewport.getCamera();

        float playerRelPosX = (controlling.position.x - c.position.x) / (mapViewport.getWorldWidth()/2);
        float playerRelPosY = (controlling.position.y - c.position.y) / (mapViewport.getWorldHeight()/2);

        if( Math.abs(playerRelPosX) > 0.5f ){
            float factor = playerRelPosX > 0 ? 0.5f : -0.5f;
            c.position.x = controlling.position.x - factor*(mapViewport.getWorldWidth()/2);
        }
        if( Math.abs(playerRelPosY) > 0.5f ){
            float factor = playerRelPosY > 0 ? 0.5f : -0.5f;
            c.position.y = controlling.position.y - factor*(mapViewport.getWorldHeight()/2);
        }

        // Camera bounds!
        float cl = c.position.x - mapViewport.getWorldWidth()/2;
        float cr = c.position.x + mapViewport.getWorldWidth()/2;
        float ct = c.position.y - mapViewport.getWorldHeight()/2;
        float cb = c.position.y + mapViewport.getWorldHeight()/2;

        if( cl < 0 ) c.position.x = mapViewport.getWorldWidth()/2;
        if( cr > getWidth() ) c.position.x = getWidth() - mapViewport.getWorldWidth()/2;
        if( cb < 0 ) c.position.y = mapViewport.getWorldHeight()/2;
        if( ct > getHeight() ) c.position.y = getHeight() - mapViewport.getWorldHeight()/2;

        c.update();
    }

    @Override
    public void resize(int width, int height) {
        guiViewport.update(width, height, true);
        mapViewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();

        // Stop all actions
        actions.clear();
    }
}
