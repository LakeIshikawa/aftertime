package com.mountainlake.aftertime.layers;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Lake on 19/12/2016.
 */
public class GameMapLayer {
    // The wrapped map layer
    public TiledMapTileLayer tileLayer;

    // Scroll speed
    public Vector2 scrollSpeed = new Vector2();
    public Vector2 wrapPoint = new Vector2();

    /**
     * Create a game layer from a Tiled map layer
     * @param tileLayer The layer to wrap
     */
    public GameMapLayer(TiledMapTileLayer tileLayer){
        this.tileLayer = tileLayer;
    }

    /**
     * Frame update
     * @param delta Delta time
     */
    public void update(float delta){

    }
}
