package com.mountainlake.aftertime;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultConnection;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.SmoothableGraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mountainlake.aftertime.gameobject.GameObject;

/**
 * Created by Lake on 04/01/2017.
 */
public class MapCollision implements IndexedGraph<MapCollision.MapGraphNode>, RaycastCollisionDetector<Vector2> {
    private final static Color mapGraphColor = new Color(1, 0, 0, 0.5f);

    /**
     * Smoothable path of nodes
     */
    public static class NodePath extends DefaultGraphPath<MapGraphNode> implements SmoothableGraphPath<MapGraphNode, Vector2> {
        @Override
        public Vector2 getNodePosition(int index) {
            return nodes.get(index).position;
        }
        @Override
        public void swapNodes(int index1, int index2) {
            nodes.swap(index1, index2);
        }
        @Override
        public void truncatePath(int newLength) {
            nodes.truncate(newLength);
        }
    }

    /**
     * A node of the map graph
     */
    public static class MapGraphNode {
        public int index;
        public Vector2 position = new Vector2();
        public Array<Connection<MapGraphNode>> connections = new Array<>();

        public MapGraphNode(int index, float x, float y){
            this.index = index;
            this.position.set(x, y);
        }

        public float distanceTo(MapGraphNode endNode) {
            return position.dst(endNode.position);
        }
    }

    /**
     * A connection for the nodes of the map graph
     */
    public static class MapGraphConnection extends DefaultConnection<MapGraphNode> {
        public float cost;

        public MapGraphConnection(MapGraphNode from, MapGraphNode to, float cost){
            super(from, to);
            this.cost = cost;
        }

        @Override
        public float getCost() {
            return cost;
        }
    }

    // Walkable areas
    private Array<Polygon> walkableAreas;
    private Array<Polygon> unwalkableAreas;
    // The node map
    public MapGraphNode nodes[][];
    // The total nodes
    public int numNodes = 0;

    // Temp buffers
    private Vector2 tester = new Vector2();
    private Vector2 futurepos = new Vector2();
    private Vector2 wall = new Vector2();
    private Vector2 intersection = new Vector2();
    private int bests, bests2;

    /**
     * Create a graph from a tiled map
     */
    public MapCollision(int width, int height, Array<Polygon> walkableAreas, Array<Polygon> unwalkableAreas){
        nodes = new MapGraphNode[width][height];
        this.walkableAreas = walkableAreas;
        this.unwalkableAreas = unwalkableAreas;

        // Create all walkable nodes
        for( int x=0; x<width; x++ ){
            for( int y=0; y<height; y++ ){
                MapGraphNode newNode = new MapGraphNode(numNodes++, x+0.5f, y+0.5f);

                if( isWalkable(newNode.position, 0.1f, 0.1f) ) {
                    nodes[x][y] = newNode;
                }
            }
        }

        // Connect the nodes
        for( int x=0; x<width; x++ ) {
            for (int y = 0; y < height; y++) {
                MapGraphNode n = nodes[x][y];
                if( n!= null ){
                    if( x<width-1 && nodes[x+1][y] != null ) n.connections.add(new MapGraphConnection(n, nodes[x+1][y], 1) );
                    if( x>0 && nodes[x-1][y] != null ) n.connections.add(new MapGraphConnection(n, nodes[x-1][y], 1) );
                    if( y<height-1 && nodes[x][y+1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x][y+1], 1) );
                    if( y>0 && nodes[x][y-1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x][y-1], 1) );

                    if( x<width-1 && y<height-1 && nodes[x+1][y+1] != null && nodes[x+1][y] != null && nodes[x][y+1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x+1][y+1], (float) Math.sqrt(2)) );
                    if( x<width-1 && y>0 && nodes[x+1][y-1] != null && nodes[x+1][y] != null && nodes[x][y-1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x+1][y-1], (float) Math.sqrt(2)) );
                    if( x>0 && y<height-1 && nodes[x-1][y+1] != null && nodes[x-1][y] != null && nodes[x][y+1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x-1][y+1], (float) Math.sqrt(2)) );
                    if( x>0 && y>0 && nodes[x-1][y-1] != null && nodes[x-1][y] != null && nodes[x][y-1] != null ) n.connections.add(new MapGraphConnection(n, nodes[x-1][y-1], (float) Math.sqrt(2)) );
                }
            }
        }
    }


    /**
     * Determines if the given point is walkable
     * @param point A point
     * @return Whether characters can walk on it
     */
    public boolean isWalkable(Vector2 point, float collwidth, float collheight){
        boolean canwalk = false;
        for( Polygon p : walkableAreas ){
            if( strictContains(p, point, tester, collwidth, collheight) ) {
                canwalk = true;
                break;
            }
        }
        for( Polygon p : unwalkableAreas ){
            if( gentleContains(p, point, tester, collwidth, collheight) ){
                canwalk = false;
                break;
            }
        }

        return canwalk;
    }

    /**
     * Collide character to walls, making is smoothly slide
     * @param character
     */
    public void collideChar(GameObject character, float delta) {
        // For every walkable area
        for (Polygon p : walkableAreas) {
            doCollision(character, p, delta, true);
        }

        // For every unwalkable area
        for (Polygon p : unwalkableAreas) {
            doCollision(character, p, delta, false);
        }
    }


    // Do collision with specified polygon
    private void doCollision(GameObject controlling, Polygon p, float delta, boolean walkable) {
        float x3 = controlling.position.x;
        float y3 = controlling.position.y;

        // Character collision rectangle
        float ofxv = controlling.collision.x / 2;
        float ofyv = controlling.collision.y / 2;
        float[] ofx = {ofxv, ofxv, -ofxv, -ofxv};
        float[] ofy = {-ofyv, ofyv, ofyv, -ofyv};

        // Take closest segment
        float bestd2 = Float.POSITIVE_INFINITY;
        float bestd = Float.POSITIVE_INFINITY;

        float[] tv = p.getTransformedVertices();
        for (int i = 0; i < tv.length; i += 2) {
            float x1 = tv[i % tv.length];
            float y1 = tv[(i + 1) % tv.length];
            float x2 = tv[(i + 2) % tv.length];
            float y2 = tv[(i + 3) % tv.length];

            for (int di = 0; di < 4; di++) {
                float newd = Intersector.distanceSegmentPoint(x1, y1, x2, y2, x3 + ofx[di], y3 + ofy[di]);
                if (newd < bestd) {
                    bests2 = bests;
                    bestd2 = bestd;
                    bestd = newd;
                    bests = i;
                } else if (newd < bestd2 && i!=bests) {
                    bests2 = i;
                    bestd2 = newd;
                }
            }
        }

        for (int di = 0; di < 4; di++) {
            // Collide with closest 2 segments
            futurepos.set(controlling.position).add(ofx[di] + controlling.speed.x * delta, ofy[di] + controlling.speed.y * delta);

            // Check first seg
            boolean check = walkable && !p.contains(futurepos);
            check |= (!walkable) && p.contains(futurepos);

            if (check) {
                collideSegment(controlling, p, bests, controlling.position.x + ofx[di], controlling.position.y + ofy[di], futurepos, delta);
                futurepos.set(controlling.position).add(ofx[di] + controlling.speed.x * delta, ofy[di] + controlling.speed.y * delta);
            }

            // Check second seg
            check = walkable && !p.contains(futurepos);
            check |= (!walkable) && p.contains(futurepos);
            if (check) {
                collideSegment(controlling, p, bests2, controlling.position.x + ofx[di], controlling.position.y + ofy[di], futurepos, delta);
                futurepos.set(controlling.position).add(ofx[di] + controlling.speed.x * delta, ofy[di] + controlling.speed.y * delta);
            }

            // If still collision, cannot fucking move.
            check = walkable && !p.contains(futurepos);
            check |= (!walkable) && p.contains(futurepos);
            if (check) {
                controlling.speed.setZero();
                break;
            }
        }
    }

    // Collide with a segment
    private void collideSegment(GameObject controlling, Polygon p, int i, float sx, float sy, Vector2 futurepos, float delta) {
        float[] vs = p.getTransformedVertices();
        float x1 = vs[i%vs.length];
        float y1 = vs[(i+1)%vs.length];
        float x2 = vs[(i+2)%vs.length];
        float y2 = vs[(i+3)%vs.length];
        if (Intersector.intersectSegments(x1, y1, x2, y2, sx, sy, futurepos.x, futurepos.y, intersection)) {
            // Calculate speed relative to wall's system
            wall.set(x2-x1, y2-y1);
            float wallAngle = wall.angle();
            controlling.speed.rotate(-wallAngle);
            // Set speed tangent to wall
            controlling.speed.y = 0;
            controlling.speed.rotate(wallAngle);
        }
    }

    private boolean strictContains(Polygon p, Vector2 position, Vector2 tester, float w, float h) {
        tester.set(position).add(0, h);
        if( !p.contains(tester) ) return false;
        tester.set(position).add(w, 0);
        if( !p.contains(tester) ) return false;
        tester.set(position).add(0, -h);
        if( !p.contains(tester) ) return false;
        tester.set(position).add(-w, 0);
        if( !p.contains(tester) ) return false;
        return true;
    }

    private boolean gentleContains(Polygon p, Vector2 position, Vector2 tester, float w, float h) {
        tester.set(position).add(0, h);
        if( p.contains(tester) ) return true;
        tester.set(position).add(w, 0);
        if( p.contains(tester) ) return true;
        tester.set(position).add(0, -h);
        if( p.contains(tester) ) return true;
        tester.set(position).add(-w, 0);
        if( p.contains(tester) ) return true;
        return false;
    }

    void renderCollision(ShapeRenderer shapeRenderer, GameObject controlling){
        // Closest segments
        if (walkableAreas.size > 0) {
            float[] vs = walkableAreas.peek().getTransformedVertices();
            AfterTime.i.shapeRenderer.setColor(Color.RED);
            AfterTime.i.shapeRenderer.line(vs[bests], vs[bests + 1], vs[(bests + 2) % vs.length], vs[(bests + 3) % vs.length]);
            AfterTime.i.shapeRenderer.setColor(Color.ORANGE);
            AfterTime.i.shapeRenderer.line(vs[bests2], vs[bests2 + 1], vs[(bests2 + 2) % vs.length], vs[(bests2 + 3) % vs.length]);
        }

        // Chara collision
        if( controlling != null ) {
            float w = controlling.collision.x;
            float h = controlling.collision.y;
            float x = controlling.position.x - w/2;
            float y = controlling.position.y - h/2;
            AfterTime.i.shapeRenderer.setColor(Color.BLACK);
            AfterTime.i.shapeRenderer.rect(x, y, w, h);
        }

        // Map graph
        AfterTime.i.shapeRenderer.setColor(mapGraphColor);
        AfterTime.i.shapeRenderer.set(ShapeRenderer.ShapeType.Filled);
        for(int x=0; x<nodes.length; x++) {
            for (int y=0; y<nodes[0].length; y++) {
                MapCollision.MapGraphNode n = nodes[x][y];
                if( n != null ){
                    AfterTime.i.shapeRenderer.rect(x, y, 1, 1);
                }
            }
        }
    }

    @Override
    public int getIndex(MapGraphNode node) {
        return node.index;
    }

    @Override
    public int getNodeCount() {
        return numNodes;
    }

    @Override
    public Array<Connection<MapGraphNode>> getConnections(MapCollision.MapGraphNode fromNode) {
        return fromNode.connections;
    }

    @Override
    public boolean collides(Ray<Vector2> ray) {
        boolean collides = true;
        for( Polygon p : walkableAreas ){
            if( p.contains(ray.start) && p.contains(ray.end) && !Intersector.intersectSegmentPolygon(ray.start, ray.end, p) ) {
                collides = false;
                break;
            }
        }
        for( Polygon p : unwalkableAreas ){
            if( p.contains(ray.start) || p.contains(ray.end) || Intersector.intersectSegmentPolygon(ray.start, ray.end, p) ) {
                collides = true;
                break;
            }
        }
        return collides;
    }

    @Override
    public boolean findCollision(Collision<Vector2> outputCollision, Ray<Vector2> inputRay) {
        return false;
    }
}
