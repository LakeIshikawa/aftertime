package com.mountainlake.aftertime;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap;
import com.brashmonkey.spriter.Data;
import com.brashmonkey.spriter.Drawer;
import com.brashmonkey.spriter.SCMLReader;
import com.mountainlake.aftertime.data.CharacterClass;
import com.mountainlake.aftertime.data.EnemyClass;
import com.mountainlake.aftertime.gameobject.Character;
import com.mountainlake.aftertime.gameobject.Enemy;
import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.lua.*;
import com.mountainlake.aftertime.resbuilder.ResourceBuilder;
import com.mountainlake.aftertime.spriter.LibGdxAtlasLoader;
import com.mountainlake.aftertime.spriter.LibGdxDrawer;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.CoroutineLib;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;
import org.luaj.vm2.lib.jse.JsePlatform;

public class AfterTime extends Game {
    public static AfterTime i;

    // Resource builder
    private ResourceBuilder builder;

    // Game entities
    public ObjectMap<String, Object> entities = new ObjectMap<>();

    // System atlas
    public TextureAtlas sprites;
    // Spriter data
    public Data spritesData;
    // Spriter drawer
    public Drawer<Sprite> spritesDrawer;

    // Rendering
    public SpriteBatch batch;
    public ShapeRenderer shapeRenderer;
    public Fader fader = new Fader();

    // Current BGM
    public Music currentBGM;

    // Fonts
    private ObjectMap<String, BitmapFont> fonts = new ObjectMap<>();

    // LUA engine
    public Globals globals;

    // Json
    private Json json = new Json();

    /**
     * Create specifying resource builder to use
     * @param builder
     */
    public AfterTime(ResourceBuilder builder){
        this.builder = builder;
    }

    @Override
	public void create () {
	    i = this;
	    json.setIgnoreUnknownFields(true);

	    // Build resources!
        if( builder != null ) builder.makeResources();

	    // Rendering
        batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setAutoShapeType(true);

	    // Load all sprites
        FileHandle spritesScml = Gdx.files.internal("packed/sprites.scml");
        sprites = new TextureAtlas("packed/sprites.atlas");
        spritesData = new SCMLReader(spritesScml.read()).getData();
        LibGdxAtlasLoader loader = new LibGdxAtlasLoader(AfterTime.i.spritesData, AfterTime.i.sprites);
        loader.load(spritesScml.file());
        spritesDrawer = new LibGdxDrawer(loader, batch, shapeRenderer);

        // Load system font
        loadFont("fnt_screen", "fonts/ChronoTrigger.ttf", 65);
        loadFont("fnt_map", "fonts/ChronoTrigger.ttf", 35);

        // Load lua engine
        globals = JsePlatform.standardGlobals();
        globals.load(new CoroutineLib());
        globals.set("y", new LY());
        globals.set("loadCharacter", new LLoadCharacter());
        globals.set("changeRoom", new LChangeRoom());
        globals.set("control", new LControl());
        globals.set("controlsOff", new LControlsOff());
        globals.set("spawnObject", new LSpawnObject());
        globals.set("spawnScript", new LSpawnScript());
        globals.set("NBtext", new LText());
        globals.set("NBcharText", new LCharText());
        globals.set("NBcharTextTimed", new LCharTextTimed());
        globals.set("NBfadeIn", new LFadeIn());
        globals.set("NBfadeOut", new LFadeOut());
        globals.set("NBcameraPanTo", new LCameraPanTo());
        globals.set("NBcameraPanBy", new LCameraPanBy());
        globals.set("NBcharWalkTo", new LCharWalkTo());
        globals.set("NBcharWalkBy", new LCharWalkBy());
        globals.set("setBgm", new LSetBgm());
        globals.set("destroy", new LDestroy());

        globals.set("pause", new LPause());
        globals.set("resume", new LResume());

        globals.set("fadeIn", new LBFadeIn(globals));
        globals.set("fadeOut", new LBFadeOut(globals));
        globals.set("cameraPanTo", new LBCameraPanTo(globals));
        globals.set("cameraPanBy", new LBCameraPanBy(globals));
        globals.set("charPathTo", new LBCharPathTo(globals));
        globals.set("charWalkTo", new LBCharWalkTo(globals));
        globals.set("charWalkBy", new LBCharWalkBy(globals));
        globals.set("doAnimation", new LBDoAnimation(globals));
        globals.set("wait", new LBWait(globals));
        globals.set("text", new LBText(globals));
        globals.set("charText", new LBCharText(globals));
        globals.set("charTextTimed", new LBCharTextTimed(globals));

        // Set some constants
        globals.set("RIGHT", 0);
        globals.set("UP", 1);
        globals.set("LEFT", 2);
        globals.set("DOWN", 3);


        // Load init script
        evalScript(Gdx.files.internal("start.lua").readString());
	}

    /**
     * Loads a character into the game
     * @param instanceName The instance name for the character
     * @param classpath The .json characterClass resource
     */
    public void loadCharacter(String instanceName, String classpath) {
	    CharacterClass clazz = json.fromJson(CharacterClass.class, Gdx.files.internal(classpath));
	    Character newChar = new Character(instanceName, clazz);
        entities.put(instanceName, newChar);

        // Bind to script
        globals.set(instanceName, CoerceJavaToLua.coerce(newChar));
    }

    /**
     * Loads an enemy into the game
     * @param instanceName Instance name
     * @param classpath The .json enemyClass resource
     */
    public Enemy loadEnemy(String instanceName, String classpath) {
        EnemyClass clazz = json.fromJson(EnemyClass.class, Gdx.files.internal(classpath));
        Enemy newEnemy = new Enemy(instanceName, clazz);
        entities.put(instanceName, newEnemy);

        // Bind to script
        globals.set(instanceName, CoerceJavaToLua.coerce(newEnemy));
        return newEnemy;
    }

    /**
     * Gets a character
     * @param name Name of the intance
     * @return The character
     */
    public Character getCharacter(String name) {
        return (Character) entities.get(name);
    }

    /**
     * Loads a font into the game
     * @param instanceName The instance name for the font
     * @param path The .ttf font resource
     * @param size The size of the font
     */
    public void loadFont(String instanceName, String path, int size){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(path));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.shadowOffsetX = 4;
        parameter.shadowOffsetY = 4;
        parameter.borderWidth = 2;
        parameter.borderWidth= 2;
        parameter.borderColor = Color.BLACK;
        parameter.size = size;
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose();

        fonts.put(instanceName, font);
    }

    /**
     * Gets a loaded font
     * @param name Font instance name
     * @return The font
     */
    public BitmapFont getFont(String name){
        return fonts.get(name);
    }

    /**
     * Evaluate the given lua script
     * @param script A lua script
     */
    public void evalScript(String script) {
        String coroutine = "coroutine.wrap(function() " + script + " end)()";
        globals.load(coroutine).call();
        globals.set("__co__", LuaValue.NIL);
    }

    /**
     * Evaluate an object script.  The script may contain one function for each object event.
     * @param go The game object
     * @param script The script
     */
    public void loadObjectScript(GameObject go, String script) {
        globals.set("this", CoerceJavaToLua.coerce(go));
        globals.load(script).call();
        globals.set("this", LuaValue.NIL);
    }

    /**
     * Evaluate condition
     * @param condition The condition to evaluate
     * @return The condition check result
     */
    public boolean evalCondition(String condition) {
        globals.load("__condcheck__ = " + condition).call();
        boolean result = globals.get("__condcheck__").checkboolean();
        globals.set("__condcheck__", LuaValue.NIL);
        return result;
    }

    /**
     * @return The current map camera
     */
    public Camera getMapCamera() {
        return getRoom().mapViewport.getCamera();
    }

    /**
     * @return Current room screen
     */
    public RoomScreen getRoom(){ return (RoomScreen) screen; }

    /**
     * Sets the current bgm
     * @param resource BGM resource file
     */
    public void setBgm(String resource) {
        if( currentBGM != null && currentBGM.isPlaying() ){
            currentBGM.stop();
            currentBGM.dispose();
        }

        currentBGM = Gdx.audio.newMusic(Gdx.files.internal(resource));
        currentBGM.setLooping(true);
        currentBGM.play();
    }
}
