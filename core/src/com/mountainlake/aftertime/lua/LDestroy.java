package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import com.mountainlake.aftertime.gameobject.GameObject;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 22/12/2016.
 */
public class LDestroy extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        GameObject object = (GameObject) arg.checkuserdata();
        AfterTime.i.getRoom().removeObject(object);
        return LuaValue.NONE;
    }
}
