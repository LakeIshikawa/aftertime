package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.gameobject.GameObject;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 24/12/2016.
 */
public class LPause extends OneArgFunction {

    @Override
    public LuaValue call(LuaValue arg) {
        GameObject go = (GameObject) arg.checkuserdata();
        go.pauseAction();
        return LuaValue.NIL;
    }
}
