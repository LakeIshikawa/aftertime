package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.CharTextAction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

/**
 * Created by Lake on 01/01/2017.
 */
public class LCharTextTimed extends VarArgFunction {

    @Override
    public Varargs invoke(Varargs args) {
        GameObject go = (GameObject) args.checkuserdata(1, GameObject.class);
        String text = args.arg(2).checkjstring();
        float time = (float) args.arg(3).checkdouble();
        float speed = 48;
        if( args.narg() == 4 ) speed = (float) args.arg(4).checkdouble();

        AfterTime.i.getRoom().startAction(
                new CharTextAction(false, go, text, speed, time));

        return LuaValue.NONE;
    }
}
