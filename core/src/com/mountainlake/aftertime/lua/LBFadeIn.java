package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 21/12/2016.
 */
public class LBFadeIn extends BlockingFunction {

    /**
     * Fade-in action
     */
    public class FadeInAction extends ScriptAction {
        private float fadeTime;

        /**
         * Fade-in action
         * @param fadeTime Fade time
         */
        public FadeInAction(float fadeTime) {
            super(true);
            this.fadeTime = fadeTime;
        }

        @Override
        public void onStart() {
            AfterTime.i.fader.fadeIn(fadeTime);
        }

        @Override
        public boolean onUpdate(float delta) {
            return AfterTime.i.fader.isFinished();
        }
    }

    /**
     * Create function
     * @param globals Globals instance
     */
    public LBFadeIn(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        return new FadeInAction((float)args.arg1().checkdouble());
    }
}
