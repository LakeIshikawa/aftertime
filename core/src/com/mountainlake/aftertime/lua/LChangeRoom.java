package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.Gdx;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 16/12/2016.
 */
public class LChangeRoom extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                AfterTime.i.setScreen(new RoomScreen(arg.checkjstring()));
            }
        });
        return LuaValue.NONE;
    }
}
