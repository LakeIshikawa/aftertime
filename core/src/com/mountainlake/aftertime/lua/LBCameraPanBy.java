package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.graphics.Camera;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.CameraPanAction;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 20/12/2016.
 */
public class LBCameraPanBy extends BlockingFunction {

    /**
     * Create function
     * @param globals Globals instance
     */
    public LBCameraPanBy(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        Camera c = AfterTime.i.getMapCamera();
        return new CameraPanAction(
                true,
                c.position.x + (float)args.arg(1).checkdouble(),
                c.position.y + (float)args.arg(2).checkdouble(),
                (float)args.arg(3).checkdouble());
    }
}
