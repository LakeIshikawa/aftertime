package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

/**
 * Created by Lake on 16/12/2016.
 */
public abstract class BlockingFunction extends VarArgFunction {

    // Globals
    private Globals globals;

    // Specify the globals to use for yielding
    public BlockingFunction(Globals globals){
        this.globals = globals;
    }

    @Override
    public Varargs invoke(Varargs args) {
        // Create action
        ScriptAction action = onCreateAction(args);
        AfterTime.i.getRoom().startAction(action);

        // Yield
        globals.yield(LuaValue.NONE);
        return LuaValue.NONE;
    }

    // Sub-class implemented routine
    protected abstract ScriptAction onCreateAction(Varargs args);
}
