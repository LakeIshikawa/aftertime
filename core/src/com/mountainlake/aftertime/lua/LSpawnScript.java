package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.Gdx;
import com.mountainlake.aftertime.AfterTime;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 20/12/2016.
 */
public class LSpawnScript extends OneArgFunction {

    @Override
    public LuaValue call(LuaValue arg) {
        String script = arg.checkjstring();
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                AfterTime.i.evalScript(Gdx.files.internal(script).readString());
            }
        });

        return LuaValue.NONE;
    }
}
