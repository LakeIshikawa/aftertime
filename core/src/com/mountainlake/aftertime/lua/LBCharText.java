package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.CharTextAction;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 01/01/2017.
 */
public class LBCharText extends BlockingFunction {

    /**
     * Create function
     * @param globals
     */
    public LBCharText(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        GameObject go = (GameObject) args.arg(1).checkuserdata(GameObject.class);
        String text = args.arg(2).checkjstring();
        float speed = 48;
        if( args.narg() == 3 ) speed = (float) args.arg(3).checkdouble();

        return new CharTextAction(true, go, text, speed, -1);
    }
}
