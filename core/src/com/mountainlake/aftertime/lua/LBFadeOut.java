package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 21/12/2016.
 */
public class LBFadeOut extends BlockingFunction {

    /**
     * Fade-out action
     */
    public class FadeOutAction extends ScriptAction {
        private float fadeTime;

        /**
         * Fade-in action
         * @param fadeTime Fade time
         */
        public FadeOutAction(float fadeTime) {
            super(true);
            this.fadeTime = fadeTime;
        }

        @Override
        public void onStart() {
            AfterTime.i.fader.fadeOut(fadeTime);
        }

        @Override
        public boolean onUpdate(float delta) {
            return AfterTime.i.fader.isFinished();
        }
    }

    /**
     * Create function
     * @param globals Globals instance
     */
    public LBFadeOut(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        return new FadeOutAction((float)args.arg1().checkdouble());
    }
}
