package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 04/01/2017.
 */
public class LY extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        float y = (float) arg.checkdouble(1);
        return LuaValue.valueOf(AfterTime.i.getRoom().getHeight() - y);
    }
}
