package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 20/12/2016.
 */
public class LBWait extends BlockingFunction {
    /**
     * Wait action
     */
    public class WaitAction extends ScriptAction {
        private float time = 0;
        private float waitTime;

        public WaitAction(float waitTime){
            super(true);
            this.waitTime = waitTime;
        }

        @Override
        public void onStart() {}

        @Override
        public boolean onUpdate(float delta) {
            time += delta;
            return time >= waitTime;
        }
    }

    public LBWait(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        return new WaitAction((float) args.checkdouble(1));
    }
}
