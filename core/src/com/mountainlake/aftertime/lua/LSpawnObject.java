package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import com.mountainlake.aftertime.gameobject.GameObject;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

/**
 * Created by Lake on 20/12/2016.
 */
public class LSpawnObject extends VarArgFunction {

    @Override
    public Varargs invoke(Varargs args) {
        String instanceName = args.checkjstring(1);
        String entity = args.checkjstring(2);
        float x = (float) args.checkdouble(3);
        float y = (float) args.checkdouble(4);

        GameObject o = new GameObject(instanceName, entity);
        o.position.set(x, y);
        AfterTime.i.getRoom().addObject(instanceName, o);

        return CoerceJavaToLua.coerce(o);
    }
}
