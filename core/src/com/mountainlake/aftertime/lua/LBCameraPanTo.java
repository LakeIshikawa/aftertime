package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.actions.CameraPanAction;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 20/12/2016.
 */
public class LBCameraPanTo extends BlockingFunction {

    /**
     * Create function
     * @param globals Globals instance
     */
    public LBCameraPanTo(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        return new CameraPanAction(
                true,
                (float)args.arg(1).checkdouble(),
                (float)args.arg(2).checkdouble(),
                (float)args.arg(3).checkdouble());
    }
}
