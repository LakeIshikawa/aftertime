package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.CharWalkToAction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

/**
 * Created by Lake on 22/12/2016.
 */
public class LCharWalkTo extends VarArgFunction {


    @Override
    public Varargs invoke(Varargs args) {
        GameObject character = (GameObject) args.checkuserdata(1, GameObject.class);
        float x = (float) args.checkdouble(2);
        float y = (float) args.checkdouble(3);
        float speed = 3.5f;
        if( args.narg() > 3 ) {
            speed = (float) args.checkdouble(4);
        }

        AfterTime.i.getRoom().startAction(
                new CharWalkToAction(
                        false,
                        character,
                        x,
                        y,
                        speed)
        );
        return LuaValue.NONE;
    }
}
