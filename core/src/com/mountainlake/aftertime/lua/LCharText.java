package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.CharTextAction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.VarArgFunction;

/**
 * Created by Lake on 01/01/2017.
 */
public class LCharText extends VarArgFunction {

    @Override
    public Varargs invoke(Varargs args) {
        GameObject go = (GameObject) args.checkuserdata(1, GameObject.class);
        String text = args.arg(2).checkjstring();
        float speed = 48;
        if( args.narg() == 3 ) speed = (float) args.arg(3).checkdouble();

        AfterTime.i.getRoom().startAction(
                new CharTextAction(false, go, text, speed, -1));

        return LuaValue.NONE;
    }
}
