package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.Gdx;
import com.mountainlake.aftertime.AfterTime;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.TwoArgFunction;

/**
 * Created by Lake on 16/12/2016.
 */
public class LLoadCharacter extends TwoArgFunction {
    @Override
    public LuaValue call(LuaValue arg1, LuaValue arg2) {
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                AfterTime.i.loadCharacter(arg1.checkjstring(), arg2.checkjstring());
            }
        });
        return LuaValue.NONE;
    }
}
