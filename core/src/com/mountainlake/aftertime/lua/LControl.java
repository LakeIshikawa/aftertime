package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import com.mountainlake.aftertime.gameobject.Character;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 19/12/2016.
 */
public class LControl extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        Character c = (Character) arg.checkuserdata(Character.class);
        AfterTime.i.getRoom().controlling = c;
        return LuaValue.NONE;
    }
}
