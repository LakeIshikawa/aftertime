package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 22/12/2016.
 */
public class LSetBgm extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        AfterTime.i.setBgm(arg.checkjstring());
        return LuaValue.NONE;
    }
}
