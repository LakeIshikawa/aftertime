package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 22/12/2016.
 */
public class LBDoAnimation extends BlockingFunction {

    /**
     * Action for doing an animation once
     */
    public static class DoAnimationAction extends ScriptAction {
        private GameObject object;
        private String animation;
        private boolean finished;

        public DoAnimationAction(GameObject object, String animation){
            super(true);
            this.object = object;
            this.animation = animation;
        }

        @Override
        public void onStart() {
            object.setAnimation(animation, new Runnable() {
                @Override
                public void run() {
                    finished = true;
                }
            });
        }

        @Override
        public boolean onUpdate(float delta) {
            return finished;
        }
    }

    /**
     * Create function
     * @param globals Globals
     */
    public LBDoAnimation(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        GameObject object = (GameObject) args.arg(1).checkuserdata();
        String animation = args.arg(2).checkjstring();
        return new DoAnimationAction(object, animation);
    }
}
