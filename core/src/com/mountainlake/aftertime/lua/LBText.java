package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.Array;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.TextWindow;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 21/12/2016.
 *
 * Text and wait for key press
 */
public class LBText extends BlockingFunction {

    /**
     * Text and wait key action
     */
    public class TextAction extends ScriptAction {
        private Array<String> sentences;
        private int current = 0;

        public TextAction(Array<String> sentences) {
            super(true);
            this.sentences = sentences;
        }

        @Override
        public void onStart() {
            AfterTime.i.getRoom().textWindow.show(sentences.get(current));
        }

        @Override
        public boolean onUpdate(float delta) {
            TextWindow tw = AfterTime.i.getRoom().textWindow;
            if(Gdx.input.isKeyJustPressed(Input.Keys.X) && tw.isReady()){
                current++;
                if( current < sentences.size ) {
                    tw.hotSwap(sentences.get(current));
                } else {
                    tw.hide();
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Create function
     * @param globals
     */
    public LBText(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        Array<String> sentences = new Array<>();
        for( int i=0; i<args.narg(); i++){
            sentences.add(args.checkjstring(i+1));
        }
        return new TextAction(sentences);
    }
}
