package com.mountainlake.aftertime.lua;

import com.badlogic.gdx.graphics.Camera;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.CameraPanAction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ThreeArgFunction;

/**
 * Created by Lake on 20/12/2016.
 */
public class LCameraPanBy extends ThreeArgFunction {
    @Override
    public LuaValue call(LuaValue arg1, LuaValue arg2, LuaValue arg3) {
        Camera c = AfterTime.i.getMapCamera();
        AfterTime.i.getRoom().startAction(new CameraPanAction(
                false,
                c.position.x + (float)arg1.checkdouble(),
                c.position.y + (float)arg2.checkdouble(),
                (float)arg3.checkdouble()
        ));
        return LuaValue.NONE;
    }
}
