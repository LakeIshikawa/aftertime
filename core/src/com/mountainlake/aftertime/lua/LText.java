package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 16/12/2016.
 */
public class LText extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        AfterTime.i.getRoom().textWindow.show(arg.checkjstring());
        return LuaValue.NONE;
    }
}
