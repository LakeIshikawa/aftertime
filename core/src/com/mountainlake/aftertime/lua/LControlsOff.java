package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.RoomScreen;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.ZeroArgFunction;

/**
 * Created by Lake on 26/12/2016.
 */
public class LControlsOff extends ZeroArgFunction {
    @Override
    public LuaValue call() {
        AfterTime.i.getRoom().controlling = null;
        return LuaValue.NONE;
    }
}
