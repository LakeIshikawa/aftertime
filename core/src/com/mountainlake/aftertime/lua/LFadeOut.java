package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.AfterTime;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.OneArgFunction;

/**
 * Created by Lake on 20/12/2016.
 */
public class LFadeOut extends OneArgFunction {
    @Override
    public LuaValue call(LuaValue arg) {
        AfterTime.i.fader.fadeOut((float) arg.checkdouble());
        return LuaValue.NONE;
    }
}
