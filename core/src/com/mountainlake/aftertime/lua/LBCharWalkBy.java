package com.mountainlake.aftertime.lua;

import com.mountainlake.aftertime.gameobject.GameObject;
import com.mountainlake.aftertime.actions.CharWalkToAction;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.Globals;
import org.luaj.vm2.Varargs;

/**
 * Created by Lake on 22/12/2016.
 */
public class LBCharWalkBy extends LBCharWalkTo {
    /**
     * Create function
     *
     * @param globals Globals instance
     */
    public LBCharWalkBy(Globals globals) {
        super(globals);
    }

    @Override
    protected ScriptAction onCreateAction(Varargs args) {
        GameObject character = (GameObject) args.checkuserdata(1, GameObject.class);
        float x = (float) args.checkdouble(2);
        float y = (float) args.checkdouble(3);
        float speed = 3.5f;
        if( args.narg() > 3 ) {
            speed = (float) args.checkdouble(4);
        }
        return new CharWalkToAction(true, character, character.position.x + x, character.position.y + y, speed);
    }
}
