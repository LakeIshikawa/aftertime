package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.math.Vector2;
import com.mountainlake.aftertime.gameobject.GameObject;

/**
 * Walk to action
 */
public class CharWalkToAction extends ScriptAction {
    private GameObject character;
    private Vector2 targetPos = new Vector2();
    private Vector2 startPos = new Vector2();
    private float speed;
    private float time;
    private float totTime;

    /**
     * Create a walk to action
     * @param character Character to walk
     * @param x Target X
     * @param y Target Y
     * @param speed
     */
    public CharWalkToAction(boolean blocking, GameObject character, float x, float y, float speed) {
        super(blocking);
        this.character = character;
        this.targetPos.set(x, y);
        this.startPos.set(character.position);
        this.speed = speed;
    }

    @Override
    public void onStart() {
        // Set animation
        character.facing = (int) ((new Vector2(targetPos).sub(startPos).angle()+45.0f) /90.0f) % 4;
        character.setDirectionalAnimation("walk");

        // Set speed
        character.speed
                .set(targetPos)
                .sub(startPos)
                .nor()
                .scl(speed);

        // Calculate time
        totTime = targetPos.dst(startPos) / speed;
    }

    @Override
    public boolean onUpdate(float delta) {
        time += delta;

        // Arrived
        if( time >= totTime ){
            // Stop the caracter
            character.position.set(targetPos);
            character.speed.setZero();
            character.setDirectionalAnimation("idle");
            return true;
        }

        return false;
    }
}
