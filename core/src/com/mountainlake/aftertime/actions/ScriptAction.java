package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.gameobject.GameObject;
import org.luaj.vm2.LuaThread;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

/**
 * Created by Lake on 22/12/2016.
 */ // A multi-frame action
public abstract class ScriptAction {
    // Calling thread
    public LuaThread thread;
    // Owner
    public GameObject owner;

    // Paused
    public boolean paused = false;

    /**
     * Initialize the action
     */
    public ScriptAction(boolean blocking) {
        if( blocking ) {
            this.thread = AfterTime.i.globals.running;
            LuaValue v = AfterTime.i.globals.get("this");
            if( v != LuaValue.NIL ) {
                owner = (GameObject)v.touserdata();
                owner.currentAction = this;
            }
        }
    }

    /**
     * Frame update
     *
     * @param delta Frame time
     * @return true-action finished
     */
    public boolean update(float delta) {
        if( paused ) return false;

        if (onUpdate(delta)) {
            if( thread != null ){
                if( owner != null ){
                    AfterTime.i.globals.set("this", CoerceJavaToLua.coerce(owner));
                }
                thread.resume(LuaValue.NONE);
                AfterTime.i.globals.set("this", LuaValue.NIL);
            }
            return true;
        }
        return false;
    }

    // Updation method
    public abstract void onStart();

    public abstract boolean onUpdate(float delta);
    public void onRender(SpriteBatch batch){}
    public void onRenderGUI(SpriteBatch batch){}
    public void onRenderDebug(ShapeRenderer shapeRenderer){}
}
