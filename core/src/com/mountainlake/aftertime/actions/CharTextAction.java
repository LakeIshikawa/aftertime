package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.gameobject.GameObject;

/**
 * Created by Lake on 01/01/2017.
 */
public class CharTextAction extends ScriptAction {
    private static final int TAREA_W = 600;
    private static final float LINE_HEIGHT_FACTOR = 1.2f;

    // Parameters
    private GameObject character;
    private String text;
    private float textSpeed;
    private float time;

    // State
    private float totWidth;
    private Vector3 charpos = new Vector3();
    private BitmapFont font;
    private Array<String> lines = new Array<>();
    private GlyphLayout wordLayout = new GlyphLayout();
    private float charsToWrite;
    private float curTime;

    /**
     * Initialize the action
     *
     * @param blocking
     */
    public CharTextAction(boolean blocking, GameObject character, String text, float textSpeed, float time) {
        super(blocking);
        this.character = character;
        this.text = text;
        this.textSpeed = textSpeed;
        this.time = time;
    }

    @Override
    public void onStart() {
        font = AfterTime.i.getFont("fnt_map");

        // Format
        int x=0;
        for( String line : text.split("\n")) {
            StringBuilder builder = new StringBuilder();
            String[] words = line.split(" ");
            float maxHeight = 0;
            for (int w = 0; w < words.length; w++) {
                String spacedWord = words[w]+" ";
                wordLayout.setText(font, spacedWord);
                x += wordLayout.width;
                maxHeight = Math.max(maxHeight, wordLayout.height);

                // Split the line
                if( x >= TAREA_W ){
                    lines.add(builder.toString());
                    builder = new StringBuilder(spacedWord);

                    totWidth = x - wordLayout.width;
                    maxHeight = wordLayout.height;
                    x = (int) wordLayout.width;
                } else {
                    builder.append(spacedWord);
                }
            }

            lines.add(builder.toString());
            totWidth = Math.max(totWidth, x);
            x = 0;
        }
    }

    @Override
    public boolean onUpdate(float delta) {
        // Animate
        if( text != null && charsToWrite < text.length() ) {
            charsToWrite += delta * textSpeed;
            if( charsToWrite > text.length() ) charsToWrite = text.length();
        }

        // Timed
        if( time != -1 ) {
            curTime += delta;
            return curTime > time;
        } else {
            return Gdx.input.isKeyJustPressed(Input.Keys.X) && isReady();
        }
    }

    @Override
    public void onRenderGUI(SpriteBatch batch){
        int chars = 0;
        int tw = TAREA_W;

        // Calculate text position
        Viewport mapVp = AfterTime.i.getRoom().mapViewport;
        charpos.set(character.position.x, character.position.y, 0);
        mapVp.project(charpos);

        float totHeight = (font.getLineHeight() * LINE_HEIGHT_FACTOR * lines.size);
        float scl = AfterTime.i.getRoom().getMapPixelScale();
        float x = scl*character.getTextPosition().x + charpos.x - totWidth/2;
        float y = scl*character.getTextPosition().y + charpos.y + totHeight;

        // Make sure the text is inside the screen
        x = Math.min(Math.max(5, x), AfterTime.i.getRoom().guiViewport.getWorldWidth() - (5+totWidth));
        y = Math.min(Math.max(5+totHeight, y), AfterTime.i.getRoom().guiViewport.getWorldHeight() - 5);

        for( int l=0; l<lines.size; l++ ) {
            String line = lines.get(l);
            chars += line.length();

            // Last one
            if( chars >= charsToWrite ) {
                font.setColor(character.textColor);
                font.draw(batch, line,
                        x,
                        y - l * LINE_HEIGHT_FACTOR * font.getLineHeight(),
                        0,
                        (int) (charsToWrite - (chars - line.length())),
                        tw,
                        Align.bottomLeft,
                        false);
                break;
            } else {
                font.setColor(character.textColor);
                font.draw(batch, line,
                        x,
                        y - l*LINE_HEIGHT_FACTOR*font.getLineHeight(),
                        tw,
                        Align.bottomLeft,
                        false);
            }
        }
    }

    // Typing animation completed?
    public boolean isReady() {
        return charsToWrite == text.length();
    }
}
