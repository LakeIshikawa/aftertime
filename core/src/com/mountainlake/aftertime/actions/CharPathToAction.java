package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.ai.pfa.PathSmoother;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.MapCollision;
import com.mountainlake.aftertime.gameobject.GameObject;

/**
 * Walk to action
 */
public class CharPathToAction extends ScriptAction {
    private GameObject character;
    private Vector2 targetPos = new Vector2();
    private Vector2 startPos = new Vector2();
    private float speed;

    // Graph path
    private MapCollision.NodePath path = new MapCollision.NodePath();

    // Current walkto action
    private int curNode = 0;
    private CharWalkToAction curAction;

    /**
     * Create a walk to action
     * @param character Character to walk
     * @param x Target X
     * @param y Target Y
     * @param speed
     */
    public CharPathToAction(boolean blocking, GameObject character, float x, float y, float speed) {
        super(blocking);
        this.character = character;
        this.targetPos.set(x, y);
        this.startPos.set(character.position);
        this.speed = speed;
    }

    @Override
    public void onStart() {
        // Find path
        IndexedAStarPathFinder<MapCollision.MapGraphNode> pathFinder = new IndexedAStarPathFinder<>(AfterTime.i.getRoom().mapCollision);
        MapCollision.MapGraphNode start = AfterTime.i.getRoom().mapCollision.nodes[(int)startPos.x][Math.round(startPos.y)];
        MapCollision.MapGraphNode end = AfterTime.i.getRoom().mapCollision.nodes[Math.round(targetPos.x)][(int)targetPos.y];
        pathFinder.searchNodePath(start, end, (node, endNode) -> node.distanceTo(endNode), path);

        // Add chara position as first node and target as last node for smoothing
        path.nodes.insert(0, new MapCollision.MapGraphNode(-1, character.position.x, character.position.y));
        path.add(new MapCollision.MapGraphNode(-2, targetPos.x, targetPos.y));

        PathSmoother<MapCollision.MapGraphNode, Vector2> smoother = new PathSmoother<>(AfterTime.i.getRoom().mapCollision);
        smoother.smoothPath(path);

        // Do first action
        nextNode();
    }

    private boolean nextNode() {
        curNode++;
        if( curNode >= path.getCount() ) return true;

        MapCollision.MapGraphNode n = path.get(curNode);
        curAction = new CharWalkToAction(false, character, n.position.x, n.position.y, speed);
        curAction.onStart();
        return false;
    }

    @Override
    public boolean onUpdate(float delta) {
        if( curAction.update(delta) ){
            if( nextNode() ) return true;
        }
        return false;
    }

    @Override
    public void onRenderDebug(ShapeRenderer shapeRenderer){
        shapeRenderer.set(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLUE);
        MapCollision.MapGraphNode prev = null;
        for(MapCollision.MapGraphNode g : path){
            if( prev != null ){
                shapeRenderer.line(prev.position, g.position);
            }
            prev = g;
        }
    }
}
