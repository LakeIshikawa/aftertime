package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.mountainlake.aftertime.AfterTime;

/**
 * Camera pan by amount action
 */
public class CameraPanAction extends ScriptAction {
    private Vector2 moveDelta = new Vector2();
    private Vector2 startPos = new Vector2();
    private Vector2 speed = new Vector2();
    private Vector2 frameSpeed = new Vector2();
    private float moveSpeed;
    private float time;
    private float totTime;

    public CameraPanAction(boolean blocking, float x, float y, float speed) {
        super(blocking);
        this.moveSpeed = speed;
        Camera camera = AfterTime.i.getMapCamera();
        moveDelta.set(x, y).sub(camera.position.x, camera.position.y);
        startPos.set(camera.position.x, camera.position.y);
    }

    @Override
    public void onStart() {
        // Set speed
        speed
                .set(moveDelta)
                .nor()
                .scl(moveSpeed);

        // Calculate time
        totTime = moveDelta.len() / moveSpeed;
    }

    @Override
    public boolean onUpdate(float delta) {
        time += delta;
        Camera camera = AfterTime.i.getMapCamera();
        frameSpeed.set(speed).scl(delta);
        camera.position.add(frameSpeed.x, frameSpeed.y, 0);
        camera.update();
        return time >= totTime;
    }
}
