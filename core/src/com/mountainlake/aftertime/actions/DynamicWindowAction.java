package com.mountainlake.aftertime.actions;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Lake on 03/01/2017.
 */
public class DynamicWindowAction extends ScriptAction {
    // Window animation speed
    private static final float ANM_SPEED = 2.5f;

    // The window's graphics
    private NinePatch ninePatch;
    // Window position
    private float x, y, w, h;

    // Animation
    private float t=0;
    private float target;

    /**
     * Create a dynamic window with the specified graphics
     * @param patch 9-patch graphics
     */
    public DynamicWindowAction(boolean blocking, NinePatch patch, float x, float y, float w, float h){
        super(blocking);
        this.ninePatch = patch;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    /**
     * Close the window
     */
    public void close(){
        target = 0;
    }

    /**
     * @return Whether the window finished opening
     */
    public boolean isOpen(){ return t == 1; }

    @Override
    public void onStart() {
        target = 1;
    }

    /**
     * Update animation
     * @param deltaTime
     */
    public boolean onUpdate(float deltaTime){
        if( t < target ){
            t += deltaTime * ANM_SPEED;
            if( t > target ) t = target;
        }
        if( t > target ){
            t -= deltaTime * ANM_SPEED;
            if( t < target ) t = target;
        }

        // Disappear when closed
        return target == 0 && t == 0;
    }

    /**
     * Render the window
     * @param batch Batch to use
     */
    public void onRenderGUI(SpriteBatch batch){
        float dy = (h/2.0f) - t*h/2.0f;
        ninePatch.draw(batch, x, y+dy, w, h*t);
    }
}
