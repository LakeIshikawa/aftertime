package com.mountainlake.aftertime;

import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Lake on 24/12/2016.
 */
public class ActionZone {
    public Rectangle rect;
    public String script;

    /**
     * Create a new action rect
     * @param rect Zone area
     * @param script The script to evaluate on trigger
     */
    public ActionZone(Rectangle rect, String script){
        this.rect = rect;
        this.script = script;
    }
}
