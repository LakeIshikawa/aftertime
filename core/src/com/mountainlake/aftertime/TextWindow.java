package com.mountainlake.aftertime;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Lake on 15/12/2016.
 *
 * A text window
 */
public class TextWindow {
    final static int TEXT_X = 32;
    final static int TEXT_Y = 276;
    private static final int INDENT = 56;
    private static final int TAREA_W = 960;
    private static final float LINE_HEIGHT_FACTOR = 1.2f;


    // The viewport
    private Viewport viewport;

    // The bg
    private TextureRegion bg;
    // The font
    private BitmapFont font;

    // Current text
    private String text;
    private boolean indent;
    private Array<String> lines = new Array<>();
    private GlyphLayout wordLayout = new GlyphLayout();
    private float charsToWrite;

    /**
     * Create a text window
     * @param bg
     * @param viewport
     */
    public TextWindow(TextureRegion bg, BitmapFont font, Viewport viewport){
        this.viewport = viewport;
        this.bg = bg;
        this.font = font;
    }

    /**
     * Show the window
     * @param text Text to show in the window
     */
    public void show(String text){
        hotSwap(text);
    }

    /**
     * Hide the window
     */
    public void hide(){
        this.text = null;
        this.lines.clear();
    }

    /**
     * Hot-swap text without window animation
     * @param text The text to swap to
     */
    public void hotSwap(String text) {
        this.text = text;
        charsToWrite = 0;
        lines = new Array();

        // If string starts with something:, parse it as heading
        int semi = text.indexOf(":");
        if( semi != -1 ) {
            String h = text.substring(0, semi);
            if (h.matches("[a-zA-Z0-9_ ]+")) {
                indent = true;
            }
        }

        // Format
        int x=0;
        for( String line : text.split("\n")) {
            StringBuilder builder = new StringBuilder();
            String[] words = line.split(" ");
            for (int w = 0; w < words.length; w++) {
                String spacedWord = words[w]+" ";
                wordLayout.setText(font, spacedWord);
                x += wordLayout.width;

                // Split the line
                if( x >= TAREA_W - (indent ? INDENT : 0) ){
                    lines.add(builder.toString());
                    builder = new StringBuilder(spacedWord);
                    x = (int) wordLayout.width;
                } else {
                    builder.append(spacedWord);
                }
            }
            lines.add(builder.toString());
        }
    }

    /**
     * @return Whether the text animation has finished and the
     * window is ready to close
     */
    public boolean isReady() {
        return charsToWrite == text.length();
    }

    /**
     * Frame update
     * @param delta Delta time
     */
    public void update(float delta){
        if( text != null && charsToWrite < text.length() ) {
            charsToWrite += delta * 48;
            if( charsToWrite > text.length() ) charsToWrite = text.length();
        }
    }

    /**
     * Render the text window
     */
    public void renderGUI(SpriteBatch batch){
        if( text != null ){
            batch.draw(bg, 0, 0);
            int chars = 0;
            int ti = (indent ? INDENT : 0); //Text indentation
            int tw = TAREA_W - ti;
            for( int l=0; l<lines.size; l++ ) {
                String line = lines.get(l);
                chars += line.length();
                int li = (l==0 ? 0 : ti); //Line indentation

                // Last one
                if( chars >= charsToWrite ){
                    font.draw(batch, line,
                            TEXT_X + li,
                            TEXT_Y - l*LINE_HEIGHT_FACTOR*font.getLineHeight(),
                            0,
                            (int) (charsToWrite-(chars-line.length())),
                            tw,
                            Align.topLeft,
                            false);
                    break;
                } else {
                    font.draw(batch, line,
                            TEXT_X + li,
                            TEXT_Y - l*LINE_HEIGHT_FACTOR*font.getLineHeight());
                }
            }
        }
    }
}
