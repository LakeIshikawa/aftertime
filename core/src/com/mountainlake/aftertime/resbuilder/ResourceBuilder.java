package com.mountainlake.aftertime.resbuilder;

/**
 * Created by Lake on 29/12/2016.
 */
public interface ResourceBuilder {
    void makeResources();
}
