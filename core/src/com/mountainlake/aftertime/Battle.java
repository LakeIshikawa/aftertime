package com.mountainlake.aftertime;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mountainlake.aftertime.gameobject.Character;
import com.mountainlake.aftertime.gameobject.Enemy;
import com.mountainlake.aftertime.actions.DynamicWindowAction;

/**
 * Created by Lake on 03/01/2017.
 */
public class Battle {
    private static final float BWIN_HEIGHT = 300;
    private static final float BWIN_MARGIN = 20;
    private static final float PARTY_TEXT_X = BWIN_MARGIN;
    private static final float PARTY_TEXT_Y = BWIN_HEIGHT - BWIN_MARGIN;
    private static final float HP_POS = 300;

    // Room screen reference
    private RoomScreen roomScreen;

    // Party
    private Array<Character> party;
    // Enemies
    private Array<Enemy> enemies;

    // ATB window
    private DynamicWindowAction atbWindow;

    // Font
    private BitmapFont statsFont;

    /**
     * Creates a battle
     * @param roomScreen
     * @param enemies The enemies to fight
     */
    public Battle(RoomScreen roomScreen, Array<Character> party, Array<Enemy> enemies) {
        this.roomScreen = roomScreen;
        this.party = party;
        this.enemies = enemies;

        statsFont = AfterTime.i.getFont("fnt_screen");
    }

    /**
     * Set up battle screen
     */
    public void onStart(){
        // Stop controlling character
        roomScreen.controlling.speed.setZero();
        roomScreen.controlling.setDirectionalAnimation("idle");
        roomScreen.controlling = null;

        // Spawn atb window
        atbWindow = new DynamicWindowAction(
                false,
                AfterTime.i.sprites.createPatch("system/atbwindow"),
                0, 0,
                roomScreen.guiViewport.getWorldWidth(),
                300);
        roomScreen.startAction(atbWindow);
    }

    /**
     * Render battle gui
     * @param batch The screen batch to use
     */
    public void renderGUI(SpriteBatch batch){
        if( !atbWindow.isOpen() ) return;

        // Render character stats
        for( int c=0; c<party.size; c++ ){
            Character chara = party.get(c);
            drawCharaStats(batch, chara, PARTY_TEXT_X, PARTY_TEXT_Y + c*statsFont.getLineHeight());
        }
    }

    // Draw chara stats line
    private void drawCharaStats(SpriteBatch batch ,Character chara, float x, float y) {
        // Name
        statsFont.draw(batch, chara.characterClass.name, x, y, roomScreen.guiViewport.getWorldWidth(), Align.topLeft, false);
        // HP
        statsFont.draw(batch, "386/386", x + HP_POS, y, roomScreen.guiViewport.getWorldWidth(), Align.topLeft, false);
    }
}
