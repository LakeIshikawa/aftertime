package com.mountainlake.aftertime.gameobject;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

/**
 * Created by Lake on 14/12/2016.
 */
public class TileObject extends GameObject {
    // The tilemap renderer
    private OrthogonalTiledMapRenderer mapRenderer;
    // The layer to render
    private TiledMapTileLayer layer;

    /**
     * Create an object made of a tile layer
     * @param layer
     */
    public TileObject(TiledMapTileLayer layer, OrthogonalTiledMapRenderer mapRenderer){
        super(layer.getName());
        this.layer = layer;
        this.mapRenderer = mapRenderer;

        // Calculate y position
        int minY = Integer.MAX_VALUE;
        for( int x=0; x<layer.getWidth(); x++ ){
            for( int y=0; y<layer.getWidth(); y++ ){
                TiledMapTileLayer.Cell c = layer.getCell(x, y);
                if( c != null ){
                    minY = Math.min(minY, y);
                }
            }
        }

        position.y = minY;
    }

    @Override
    public void render(SpriteBatch batch, int tileSize) {
        mapRenderer.renderTileLayer(layer);
    }
}
