package com.mountainlake.aftertime.gameobject;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.mountainlake.aftertime.data.CharAbilityClass;
import com.mountainlake.aftertime.data.CharacterClass;

/**
 * Created by Lake on 14/12/2016.
 */
public class Character extends GameObject {
    // The character data
    public CharacterClass characterClass;

    // Learned abilities
    public Array<CharAbilityClass> abilities = new Array<>();
    // Equipped ability
    public CharAbilityClass equipAbility;


    /**
     * Create a character instance
     * @param charClass The class of the character
     */
    public Character(String instanceName, CharacterClass charClass){
        super(instanceName, charClass.anm);
        this.characterClass = charClass;

        // Set text color
        textColor = Color.valueOf(characterClass.textcolor);
    }
}
