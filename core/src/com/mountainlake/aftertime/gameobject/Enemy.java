package com.mountainlake.aftertime.gameobject;

import com.mountainlake.aftertime.data.EnemyClass;

/**
 * Created by Lake on 02/01/2017.
 */
public class Enemy extends GameObject {
    // The enemy data
    private EnemyClass enemyClass;

    /**
     * Create an enemy with given instance name and entity
     * @param instanceName The instance name
     * @param enemyClass The enemy class
     */
    public Enemy(String instanceName, EnemyClass enemyClass) {
        super(instanceName, enemyClass.anm);
        this.enemyClass = enemyClass;
    }
}
