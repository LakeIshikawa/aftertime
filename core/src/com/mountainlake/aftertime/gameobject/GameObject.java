package com.mountainlake.aftertime.gameobject;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;
import com.brashmonkey.spriter.*;
import com.mountainlake.aftertime.AfterTime;
import com.mountainlake.aftertime.actions.ScriptAction;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.CoerceJavaToLua;

/**
 * Created by Lake on 14/12/2016.
 *
 * Basic game object with a spriter entity for animation
 */
public class GameObject implements Comparable<GameObject>, Player.PlayerListener {
    // Instance name
    public String instanceName;
    // Position
    public Vector2 position = new Vector2();
    // Height
    public float z = 0;
    // Speed
    public Vector2 speed = new Vector2();
    // Facing direction (0=right 1=up...)
    public int facing = 3;

    // Lifetime
    public float lifetime = Float.POSITIVE_INFINITY;
    public float time;

    // Current action
    public ScriptAction currentAction;
    private ScriptAction pausedAction;

    // Events
    public ObjectMap<String, LuaFunction> events = new ObjectMap<>();


    // The spriter animation player
    private Player player;
    // Animation callback
    private Runnable callback;

    // Text position
    private Vector2 textPosition = new Vector2();
    // Text color
    public Color textColor = Color.WHITE;

    // Basic Collision
    public Vector2 collision = new Vector2();
    public Rectangle worldCollision = new Rectangle();

    // Frame-speed
    private Vector2 frameSpeed = new Vector2();

    /**
     * Create a game object with no animation
     */
    public GameObject(String instanceName){
        this.instanceName = instanceName;
        player = null;
    }

    /**
     * Create a GameObject with the specified entity
     * @param entity The name of the spriter's entity for animations
     */
    public GameObject(String instanceName, String entity){
        this.instanceName = instanceName;
        player = new Player(AfterTime.i.spritesData.getEntity(entity));
        player.addListener(this);

        // Set idle or idle_down animation if present
        setDirectionalAnimation("idle");

        // Grab text position
        try {
            Timeline.Key.Object o = player.getObject("text");
            textPosition.set(o.position.x, o.position.y);
        } catch (Exception e){
            // Forget it
        }

        // Grab collision
        try {
            Timeline.Key.Object o = player.getObject("collision");
            float tileSize = AfterTime.i.getRoom().getTileSize();
            collision.set(o.position.x*2 / tileSize, o.position.y*2 / tileSize);
        } catch (Exception e){
            // Forget it
        }
    }

    /**
     * @return Text position for the object
     */
    public Vector2 getTextPosition(){ return textPosition; }

    /**
     * Set animation and callback on completion
     * @param name Animation name
     * @param callback Callback to call when animation has finished
     */
    public void setAnimation(String name, Runnable callback){
        setAnimation(name);
        this.callback = callback;
    }

    /**
     * Set a specific animation if not already playing
     * @param name Animation name
     */
    public boolean setAnimation(String name){
        if( player == null ) return false;

        try {
            player.setAnimation(name);
            return true;
        } catch (SpriterException e){
            return false;
        }
    }

    /**
     * Set animation with direction
     * The animation must be named like 'action_down' 'action_up' ...
     * @param name Action
     */
    public void setDirectionalAnimation(String name){
        boolean res;
        switch (facing) {
            case 0:
                res = setAnimation(name + "_right");
                if( !res ) setAnimation(name + "_down");
                break;
            case 1:
                res = setAnimation(name + "_up");
                if( !res ) setAnimation(name + "_left");
                break;
            case 2:
                res = setAnimation(name + "_left");
                if( !res ) setAnimation(name + "_up");
                break;
            case 3:
                res = setAnimation(name + "_down");
                if( !res ) setAnimation(name + "_right");
                break;
        }
    }

    /**
     * Set facing direction, re-setting current animation for the correct direction
     * @param direction The direction (0=right, 1=up..)
     */
    public void face(int direction){
        if( direction != facing ){
            facing = direction;
            String name = player.getAnimation().name;
            setDirectionalAnimation(name.substring(0, name.indexOf('_')));
        }
    }

    /**
     * Set the specified character map
     * @param name Character map name
     */
    public void setCharacterMap(String name){
        player.characterMap = player.getEntity().getCharacterMap(name);
    }

    /**
     * Pause current action
     */
    public void pauseAction(){
        if( currentAction != null && pausedAction == null ){
            pausedAction = currentAction;
            currentAction.paused = true;
        }
    }

    /**
     * Resume current action
     */
    public void resumeAction(){
        if( pausedAction != null ){
            pausedAction.paused = false;
            pausedAction = null;
        }
    }

    /**
     * Add an event
     * @param name Event name
     * @param function Lua function
     */
    public void addEvent(String name, LuaFunction function){
        events.put(name, function);
    }

    /**
     * Trigger event
     * @param event The event name
     */
    public void triggerEvent(String event){
        // Can't triggere events if we are paused.
        if( pausedAction != null ) return;

        LuaFunction f = events.get(event);
        if( f != null ){
            AfterTime.i.globals.set("this", CoerceJavaToLua.coerce(this));
            AfterTime.i.globals.set("__f__", CoerceJavaToLua.coerce(f));
            AfterTime.i.evalScript("__f__()");
            AfterTime.i.globals.set("this", LuaValue.NIL);
            AfterTime.i.globals.set("__f__", LuaValue.NIL);
        }
    }

    /**
     * Game Object update
     * @param deltaTime Frame time
     * @return true if the object needs to be terminated
     */
    public boolean update(float deltaTime){
        // Update position and evaluate lifetime
        frameSpeed.set(speed).scl(deltaTime);
        position.add(frameSpeed);

        // Set collision rectangle
        worldCollision.set(
                position.x - collision.x/2,
                position.y - collision.y/2,
                collision.x,
                collision.y);

        boolean finish = time >= lifetime;
        time += deltaTime;
        return finish;
    }

    /**
     * Game Object rendering
     * @param batch The sprite batch to use
     */
    public void render(SpriteBatch batch, int tileSize){
        if( player != null ) {
            player.setPosition(position.x, position.y);
            player.setScale(1.0f/tileSize);
            player.update();
            AfterTime.i.spritesDrawer.draw(player);
        }
    }

    /**
     * Compare by z -> y position
     */
    @Override
    public int compareTo(GameObject other){
        if( z > other.z ) return 1;
        if( z < other.z ) return -1;
        return position.y > other.position.y ? -1 : position.y < other.position.y ? 1 : 0;
    }

    @Override
    public void animationFinished(Animation animation) {
        if( callback != null ) {
            Runnable run = callback;
            callback = null;
            run.run();
        }
    }

    @Override
    public void animationChanged(Animation oldAnim, Animation newAnim) {
        if( callback != null ) {
            Runnable run = callback;
            callback = null;
            run.run();
        }
    }

    @Override
    public void preProcess(Player player) {}
    @Override
    public void postProcess(Player player) {}
    @Override
    public void mainlineKeyChanged(Mainline.Key prevKey, Mainline.Key newKey) {}
}
