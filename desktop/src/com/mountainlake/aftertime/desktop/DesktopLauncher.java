package com.mountainlake.aftertime.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mountainlake.aftertime.AfterTime;

import java.io.IOException;

public class DesktopLauncher {
	public static void main (String[] arg) throws IOException {
		// Launch
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1280;
		config.height = 960;
		new LwjglApplication(new AfterTime(new DesktopResourceBuilder()), config);
	}
}
