package com.mountainlake.aftertime.desktop;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.*;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.mountainlake.aftertime.resbuilder.ResourceBuilder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;

/**
 * Created by Lake on 15/12/2016.
 */
public class DesktopResourceBuilder implements ResourceBuilder {

    public void makeResources() {
        try {
            // Pack sprites
            TexturePacker.Settings settings = new TexturePacker.Settings();
            settings.maxWidth = 4096;
            settings.maxHeight = 4096;
            settings.filterMag = Texture.TextureFilter.Nearest;
            settings.filterMin = Texture.TextureFilter.Nearest;
            settings.stripWhitespaceX = true;
            settings.stripWhitespaceY = true;
            settings.flattenPaths = false;
            settings.combineSubdirectories = true;

            // Check if packing is needed
            File atlasPath = new File("./packed/sprites.atlas");
            if (!atlasPath.exists()) {
                TexturePacker.process(settings, "./resources/sprites/", "./packed/", "sprites.atlas");
            }

            // Copy .scml
            Files.copy(Paths.get("./resources/sprites/sprites.scml"), Paths.get("./packed/sprites.scml"), StandardCopyOption.REPLACE_EXISTING);

            // Maps
            processMaps();
            copyFiles(new File("resources/maps/"), new File("packed/maps/"), "", ".tmx");

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    // Process maps
    private void processMaps() throws IOException {
        HashSet<String> wrote = new HashSet<>();
        File tmp = new File("resources/maps/tmp/");

        // Process maps
        File tilesPath = new File("resources/maps/");
        for (File f : tilesPath.listFiles()) {
            if (f.getName().endsWith(".tmx")) {

                String mapname = f.getName().substring(0, f.getName().lastIndexOf("."));

                // Check if processing is needed
                File atlasPath = new File("packed/maps/"+mapname+".atlas");
                if( atlasPath.exists() ){
                    continue;
                }

                TiledMap map = new TmxMapLoader().load(f.getPath());
                for(MapLayer l : map.getLayers()){
                    if( l instanceof TiledMapTileLayer ){
                        TiledMapTileLayer tl = (TiledMapTileLayer) l;

                        // Scan all placed tiles
                        for(int x=0; x<tl.getWidth(); x++){
                            for(int y=0; y<tl.getHeight(); y++){
                                TiledMapTileLayer.Cell c = tl.getCell(x, y);
                                if( c != null ){
                                    // Animated tile, add all others too
                                    if( c.getTile() instanceof AnimatedTiledMapTile ){
                                        AnimatedTiledMapTile at = (AnimatedTiledMapTile) c.getTile();
                                        for(StaticTiledMapTile t : at.getFrameTiles()){
                                            addTile(map, t, tl, wrote, tmp);
                                        }
                                    } else {
                                        addTile(map, c.getTile(), tl, wrote, tmp);
                                    }
                                }
                            }
                        }
                    }
                }


                // Pack them
                TexturePacker.Settings settings = new TexturePacker.Settings();
                settings.maxWidth = 4096;
                settings.maxHeight = 4096;
                settings.filterMag = Texture.TextureFilter.Nearest;
                settings.filterMin = Texture.TextureFilter.Nearest;
                settings.flattenPaths = false;
                settings.paddingX = 2;
                settings.paddingY = 2;
                settings.duplicatePadding = true;

                // Check if packing is needed
                TexturePacker.process(settings, tmp.getPath(), "./packed/maps/", mapname+".atlas");

                // Remove tmp folder
                String[]entries = tmp.list();
                for(String s: entries){
                    File currentFile = new File(tmp.getPath(),s);
                    currentFile.delete();
                }
            }

            tmp.delete();
        }
    }

    private void addTile(TiledMap map, TiledMapTile tile,
                         TiledMapTileLayer tl, HashSet<String> wrote,
                         File tmp) throws IOException {
        // Find tileset
        int id = tile.getId();
        TiledMapTileSet prev = null;
        for(TiledMapTileSet ts : map.getTileSets()) {
            int fid = ts.getProperties().get("firstgid", Integer.class);
            if( id < fid ){
                break;
            }
            prev = ts;
        }

        String outname = prev.getName() + "_" + String.format("%04d", (id-1)) + ".png";
        if( wrote.contains(outname) ) return;
        wrote.add(outname);

        TextureRegion textureRegion = tile.getTextureRegion();
        Pixmap out = new Pixmap((int)tl.getTileWidth(), (int)tl.getTileHeight(), Pixmap.Format.RGBA8888);
        Texture texture = textureRegion.getTexture();
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }
        Pixmap pixmap = texture.getTextureData().consumePixmap();
        for (int rx = 0; rx < textureRegion.getRegionWidth(); rx++) {
            for (int ry = 0; ry < textureRegion.getRegionHeight(); ry++) {
                int colorInt = pixmap.getPixel(textureRegion.getRegionX() + rx, textureRegion.getRegionY() + ry);
                out.drawPixel(rx, ry, colorInt);
            }
        }

        // Save the pixmap
        byte[] png = PNG.toPNG(out);
        tmp.mkdirs();
        File fout = new File(tmp, outname);
        OutputStream os = new FileOutputStream(fout);
        os.write(png);
        os.close();

        pixmap.dispose();
        out.dispose();
    }

    // Check recursively if an image file was modified later than the atlas last modification
    private static boolean fileModified(File f, long atlasModified) throws IOException {
        if( f.isDirectory() ) {
            // Check every file modification
            for (File c : f.listFiles()) {
                if (fileModified(c, atlasModified)) return true;
            }
        }

        String mimetype = Files.probeContentType(f.toPath());
        return mimetype != null
                && mimetype.split("/")[0].equalsIgnoreCase("image")
                && f.lastModified() > atlasModified;
    }

    // Copy preserving folder structure
    private void copyFiles(File from, File to, String path, String extension) throws IOException {
        for( File f : from.listFiles() ){
            if( f.isDirectory() ) copyFiles(f, to, path+f.getName()+"/", extension);
            else if( f.getName().endsWith(extension) ){
                File output = new File(to, path + f.getName());
                output.getParentFile().mkdirs();
                Files.copy(Paths.get(f.toURI()), Paths.get(output.toURI()), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
}
